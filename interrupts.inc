(* declare your interrupt routines here *)

procedure dli;assembler;interrupt;
asm {
    pha ; store registers
    sta wsync
    mva #$0f atari.colpf1
    mva atari.color4 atari.colpf2 
    :7 sta wsync
    mva color_dashfont atari.colpf1
    pla ; restore registers
};
end;


procedure vbl;assembler;interrupt;
asm {
    phr ; store registers
    
    lda musicOn
    beq no_music
   *** RMT play routine
    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY
    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY
    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY
    lda MSX
    ldy MSX+1
    jsr RMT.TRMT.PLAY


no_music

    lda musicOn
    beq no_scroll
    jsr MoveScroll

no_scroll
    lda #0
    sta atari.atract

    plr ; restore registers
    jmp $E462 ; jump to system VBL handler
};
end;

