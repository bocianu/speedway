for IMAGE in $(ls -1 *.gif)
do
    echo \*\*\* CONVERTING $IMAGE
    NAME="${IMAGE%.*}"
    convert $IMAGE[0] -colors 2 -depth 1 -negate $NAME.gray
    mv $NAME.gray $NAME.gr8
done

