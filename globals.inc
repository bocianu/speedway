var
    speedDelta: array [0..0] of shortint;
    speedTables: array [0..3] of pointer = (
        pointer(SPEED_TABLES), 
        pointer(SPEED_TABLES + $100), 
        pointer(SPEED_TABLES + $200), 
        pointer(SPEED_TABLES + $300)
    );
       
    p_tail: array [0..PLAYER_MAX] of byte;
    p_tailX: array [0..PLAYER_MAX,0..TAIL_LENGTH] of byte;
    p_tailY: array [0..PLAYER_MAX,0..TAIL_LENGTH] of byte;
    
    p_speed: array [0..PLAYER_MAX] of byte;
    p_speedX: array [0..PLAYER_MAX] of byte;
    p_speedY: array [0..PLAYER_MAX] of byte;
    
    p_angle: array [0..PLAYER_MAX] of byte;
    p_accel: array [0..PLAYER_MAX] of byte;
    p_turningSpan: array [0..PLAYER_MAX] of byte;
    
    p_color: array [0..PLAYER_MAX] of byte;
    p_colorTables: array [0..PLAYER_MAX,0..3] of byte = (
        (1,1,1,1),
        (3,3,3,3),
        (1,1,3,3),
        (3,1,3,1)
    );
    p_menuOffset: array [0..PLAYER_MAX] of byte = (50, 69, 130, 149);
    p_control: array [0..PLAYER_MAX] of word = ($D010, $D011, $D012, $D013);
    p_state: array [0..PLAYER_MAX] of byte;
    p_tailClear: array [0..PLAYER_MAX] of byte;
    p_X: array [0..PLAYER_MAX] of byte;
    p_Y: array [0..PLAYER_MAX] of byte;
    p_lap: array [0..PLAYER_MAX] of byte;
    p_lap_delay: array [0..PLAYER_MAX] of byte;
    p_startFrame: array [0..PLAYER_MAX] of word;
    p_lapFrame: array [0..PLAYER_MAX] of word;
    p_lapTime: array [0..PLAYER_MAX] of word;
    p_raceTime: array [0..PLAYER_MAX] of word;
    
    g_racePositions: array [0..PLAYER_MAX] of byte;
    g_matchPositions: array [0..PLAYER_MAX] of byte;
    g_matchPoints: array [0..PLAYER_MAX] of byte;
    g_matchRaces2Win: byte = 3;
    g_matchPoints2Win: byte = 5;
    g_matchCount: byte;
    g_time: word;
    g_bestTimes: array [0..9] of word;
    g_players: byte = 2;
    g_controller: byte = 0;
    g_laps: byte = 3;
    g_mode: byte = 0;
    g_bannerShowTimer: byte;
        
    race_over: boolean;
    match_over: boolean;
    
    gameModes: array [0..2] of String = (
                          'Quick Race       ',
                          'M'*'atch:    Races ',
                          'M'*'atch:     Points'
    );
    playersMenu: string = 'P1   P2   P3   P4'~; 
    
    controllers: array [0..1] of string = (
                          'Joy1 Joy2 Keys'~,
                          'MultiJoy      '~
    );
    places: array [0..5] of String = (
                          '1st Place: ',
                          '2nd Place: ',
                          '3rd Place: ',
                          '4th Place: ',
                          '---        ',
                          'DISQUALIFIED'*
    );
    crashed : TString = 'DISQUALIFIED'~*;
    raceResultsLogo: array [0..195] of byte = (
    $80, $80, $4f, $cc, $80, $4f, $cc, $80, $4f, $80, $80, $59, $00, $00, $00, $80,
    $80, $4f, $80, $80, $59, $cc, $80, $4f, $80, $00, $59, $80, $00, $d9, $80, $80,
    $59, $cc, $80, $4f, $00, $00, $00, $00, $80, $00, $59, $80, $00, $59, $80, $00,
    $4c, $80, $00, $00, $00, $00, $00, $80, $00, $59, $80, $00, $00, $80, $00, $4c,
    $80, $00, $59, $80, $00, $00, $d9, $59, $00, $80, $00, $4c, $00, $00, $00, $00,
    $80, $80, $4c, $80, $80, $59, $80, $00, $00, $80, $80, $59, $00, $00, $00, $80,
    $80, $4c, $80, $80, $59, $cf, $80, $4f, $80, $00, $59, $80, $00, $00, $d9, $59,
    $00, $cf, $80, $4f, $00, $00, $00, $00, $80, $4b, $4f, $80, $00, $59, $80, $00,
    $4f, $80, $00, $00, $00, $00, $00, $80, $4b, $4f, $80, $00, $00, $55, $00, $59,
    $80, $00, $59, $80, $00, $00, $d9, $59, $00, $55, $00, $59, $00, $00, $00, $00,
    $80, $00, $59, $80, $00, $59, $cf, $80, $4c, $80, $80, $59, $00, $00, $00, $80,
    $00, $59, $80, $80, $59, $cf, $80, $4c, $cf, $80, $4c, $80, $80, $59, $d9, $59,
    $00, $cf, $80, $4c
    );
    
    matchResultsLogo: array [0..199] of byte = (
    $d9, $cb, $00, $49, $59, $cc, $80, $4f, $80, $80, $80, $49, $80, $cb, $d9, $59,
    $d9, $00, $00, $80, $80, $4f, $80, $80, $59, $cc, $80, $4f, $80, $00, $59, $80,
    $00, $d9, $80, $80, $59, $cc, $80, $4f, $d9, $c9, $cb, $c9, $59, $80, $00, $59,
    $00, $80, $00, $d9, $59, $4b, $d9, $59, $d9, $00, $00, $80, $00, $59, $80, $00,
    $00, $80, $00, $4c, $80, $00, $59, $80, $00, $00, $d9, $59, $00, $80, $00, $4c,
    $d9, $59, $4b, $00, $59, $80, $80, $59, $00, $80, $00, $d9, $59, $00, $d9, $80,
    $80, $00, $00, $80, $80, $4c, $80, $80, $59, $cf, $80, $4f, $80, $00, $59, $80,
    $00, $00, $d9, $59, $00, $cf, $80, $4f, $d9, $59, $00, $00, $59, $80, $00, $59,
    $00, $80, $00, $d9, $59, $49, $d9, $59, $d9, $00, $00, $80, $4b, $4f, $80, $00,
    $00, $55, $00, $59, $80, $00, $59, $80, $00, $00, $d9, $59, $00, $55, $00, $59,
    $d9, $59, $00, $00, $59, $80, $00, $59, $00, $80, $00, $4b, $80, $c9, $d9, $59,
    $d9, $00, $00, $80, $00, $59, $80, $80, $59, $cf, $80, $4c, $cf, $80, $4c, $80,
    $80, $59, $d9, $59, $00, $cf, $80, $4c
    );
const FADE_COL_MAX = 9;
var
    color_dashfont:byte;
    fadeTable:array [0..FADE_COL_MAX] of word = 
    (@color0,       @color1,        @color2,    @color3,        @color4,        @pcolr0,        @pcolr1,        @pcolr2,        @pcolr3,        @color_dashfont);
    fadeTitle:array [0..FADE_COL_MAX] of word = 
    ($0,            $0,             $f,         $0,             $f,             $0,             0,              0,              0,              $f);
    fadeGame:array [0..FADE_COL_MAX] of word =  
    (COLOR_P1,      COLOR_TRACK,    COLOR_P2,   COLOR_GREEN,    COLOR_GREEN,    COLOR_FINISH,   COLOR_GREEN,    COLOR_GREEN,    COLOR_GREEN,    $0);
    
    msx: TRMT;
    musicOn: byte = 0;
    scrollOn: byte = 0;
    isStereo: boolean;
    scrollh: byte;
    scrollx: word;
    escPressed: boolean;
    oldvbl: pointer;
    
    audctl2: byte absolute $D218;
    skstat2: byte absolute $D21F;
    aud1: array [0..3,0..1] of byte absolute $D200;
    aud2: array [0..3,0..1] of byte absolute $D210;
        
