//
// ;*** define your project wide constants here
// 
// ;*** I like to keep memory locations at top of this file
//


PMG = $5C00;
TITLEFONT = $6000;
TITLEDLIST = $6500;
MENURAM = $6550;
LOGO = $6800;
RLEDATA = $7000;
SPEED_TABLES = $7200; // $400 size
RMT_MODULE_ADDRESS = $7600; // ends at $8282;
RMT_PLAYER_ADDRESS = $8700;
SCROLL = $8B00;



// ;*** and here goes all other stuff

TAIL_LENGTH = 31;
PLAYER_MAX = 3;
GAME_SPEED = 160;  // MIN 128 - fastest
ACCEL_DELAY = 16;
BRAKE_DELAY = 10;

STATE_IDLE = 0;
STATE_DRIVING = 1;
STATE_FINISHED = 2;
STATE_CRASHED = 3;
STATE_ENDED = 4;

COLOR_GREEN = $C8;
COLOR_TRACK = $f0;
COLOR_P1 = $28;
COLOR_P2 = $86;
COLOR_FINISH = $a;

CONTROL_NORMAL = 0;
CONTROL_MULTIJOY = 1;

MODE_QUICKRACE = 0;
MODE_MATCH = 1;
MODE_RACE4POINTS = 2;
MODE_QUIT = $ff;

// fade targets:
WHITE = 0;
BLACK = 1;
TITLE = 2;
GAME = 3;


NONE = $ff;
