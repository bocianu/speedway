program madStrap;
{$librarypath '../blibs/'}
uses atari, crt, graph, math, joystick, b_utils, b_pmg, b_crt, rmt, misc; 
{$r resources.rc}

const
{$i const.inc}
{$i globals.inc}
{$i interrupts.inc}

procedure PreCalc();
var val:shortint;
    sinval: single;
    angle, speed: byte;
begin
    sdmctl := 0; // turn off antic to speed up
    for angle := 0 to 45 do begin
        sinval := Sin(degToRad(angle * 2));
        for speed := 0 to 3 do begin
            speedDelta := speedTables[speed];
            val := Round(sinval * (79 + (speed shl 4)));
            colbk := val;
            speedDelta[angle] := val;
            speedDelta[90 - angle] := val;
            speedDelta[90 + angle] := -val;
            speedDelta[180 - angle] := -val;
            speedDelta[180 + angle] := val;
        end;
    end;
end;

procedure fadeTo(target: byte);
var b,c,v,h:byte;
begin
    pause;
    for b:=0 to 15 do begin
        for c := 0 to FADE_COL_MAX do begin 
            v := peek(fadeTable[c]);
            case target of
                WHITE: begin 
                    h := v and $f0;
                    if (v and $f) < $f then inc(v)
                        else v := $0f;
                end;
                BLACK: begin 
                    h := v and $f0;
                    if (v and $f) > 0 then dec(v)
                        else v := $0;
                end;
                TITLE: begin 
                    h := fadeTitle[c] and $f0;
                    v := h or (v and $f);
                    if v > fadeTitle[c] then dec(v)
                        else v := fadeTitle[c];
                end;
                GAME:  begin 
                    h := fadeGame[c] and $f0;
                    v := h or (v and $f);
                    if v < fadeGame[c] then inc(v)
                        else v := fadeGame[c];
                end;
            end;
            poke(fadeTable[c],v);
        end;
        pause;
    end;
end;

procedure SetScrollX(x:word);
begin
    Dpoke(TITLEDLIST + $4a, SCROLL + x);
end;

procedure PutChar(mem:word;c:char;col:byte);
var chmem, w, wm:word;
    line:byte;
    m, b:byte;
    
begin
    chmem := TITLEFONT + (byte(c) shl 3) ;
    line := 0;
    repeat
        b:=Peek(chmem);
        w:=0;
        wm:=col;
        m:=%1;
        repeat
            if m and b <> 0 then 
                w := w or wm;    
            wm := wm shl 2;
            m := m shl 1;    
        until m = 0;
        Poke(mem,hi(w));
        poke(mem+1,w);
        Inc(chmem);
        Inc(mem,40);
        Inc(line);
    until line = 8;
end;

procedure PutStr(mem:word;s:TString;col:byte);
var i:byte;
begin
    for i:=0 to byte(s[0])-1 do PutChar(mem + i shl 1,s[i+1],col);
end;

procedure Banner(s:TString);
begin
    PutStr(savmsc+(36*40)+9,s,%10);    
end;

procedure ShowBanner;
begin
    PMG_hpos1 := 0;
    PMG_hpos2 := 0;
    PMG_hpos3 := 0;    
end;

procedure HideBanner;
begin
    PMG_hpos1 := 84;
    PMG_hpos2 := 84+32;
    PMG_hpos3 := 84+32+24;    
end;

procedure DisplayBanner(f:byte);
begin
    ShowBanner;
    g_bannerShowTimer := f;
end;

procedure CheckBanner;
begin
    if g_bannerShowTimer > 0 then begin
        dec(g_bannerShowTimer);
        if g_bannerShowTimer = 0 then HideBanner;
    end;
end;

procedure MoveScroll();
begin
    dec(scrollh);
    if scrollh = 3 then begin
        scrollh := 7;
        inc(scrollx);
        if scrollx > $190 then scrollx := 0;
        SetScrollX(scrollx);
    end;
    hscrol := scrollh;    
end;

procedure InitControllers(control: byte);
var dirBits:byte;
begin
    dirBits := 0;
    if control = CONTROL_MULTIJOY then dirBits := $F0;
    Poke($D302,$30);
    Poke($D300,dirBits);
    Poke($D302,$34);
end;

function FirePressed(p:byte):boolean;
begin
    result:=false;
    if g_controller = CONTROL_MULTIJOY then begin
        poke($D300,p shl 4);
    asm {   
        ldy #$06      ; czekamy 30 cykli bo
@       dey           ; niektóre interfejsy MultiJoy
        bne @-        ; mogą być nieco wolniejsze.
    };
        result := peek($D010) = 0;
    end else begin
        if p<2 then result := peek(p_control[p]) = 0;
        if p=2 then result := ((skstat and 4) = 0) and (kbcode = $21);
    end;
end;

procedure ClearTail(pnum: byte);
var tail:byte;
begin
    tail := p_tail[pnum];
    setColor(2);
    if p_tailX[pnum, tail] < $ff then PutPixel(p_tailX[pnum, tail], p_tailY[pnum, tail]);
    p_tailX[pnum, tail] := p_X[pnum];
    p_tailY[pnum, tail] := p_Y[pnum];
    p_tail[pnum] := (tail + 1) and TAIL_LENGTH;
end;

procedure DrawPlayer(pnum: byte);
var color: byte;
begin
    ClearTail(pnum);   
    color := p_color[pnum];
    setColor(p_colorTables[pnum, color]);
    p_color[pnum] := (color + 1) and 3;
    PutPixel(p_X[pnum], p_Y[pnum]);
end;

procedure ShowCrashed(pnum: byte);
var txtmem:word;
begin
    txtmem := txtmsc + p_menuOffset[pnum];
    move(crashed[1],pointer(txtmem - 4),byte(crashed[0]));
end;


function GetTimeStr(time:word):TString;
begin
    result := '00.00';
    result[1] := char(hi(time) shr 4 + 48);
    result[2] := char(hi(time) and 15 + 48);
    result[4] := char(lo(time) shr 4 + 48);
    result[5] := char(lo(time) and 15 + 48);
end;

procedure ShowLap(pnum: byte);
var txtmem:word;
    lap:byte;
begin
    txtmem := txtmsc + p_menuOffset[pnum];
    lap := p_lap[pnum];
    asm {
        mwa txtmem $fe
        ldy #0
        lda lap
        and #$0f
        ora #16
        sta ($fe),y
    };
end;

function GetTimeSpan(now,past: word):word;
begin
    asm {
        sed
        sec
        lda now
        sbc past
        sta result
        bcs @+
        add #$50
        sta result
        clc 
@       lda now+1
        sbc past+1
        sta result+1
        cld
    };
end;

procedure ShowLapTime(pnum: byte);
var txtmem,ltime:word;
begin
    txtmem := txtmsc + p_menuOffset[pnum];
    ltime := p_lapTime[pnum];
    asm {
        mwa txtmem $fe
        ldy #3
        lda ltime+1
        lsr:lsr:lsr:lsr
        ora #16
        sta ($fe),y+
        lda ltime+1
        and #$0f
        ora #16
        sta ($fe),y+
        lda #"."
        sta ($fe),y+
        lda ltime
        lsr:lsr:lsr:lsr
        ora #16
        sta ($fe),y+
        lda ltime
        and #$0f
        ora #16
        sta ($fe),y
    };
end;

procedure ShowGameTime;
var txtmem,ltime:word;
begin
    txtmem := txtmsc + 8;
    ltime := g_time;
    asm {
        mwa txtmem $fe
        ldy #3
        lda ltime+1
        lsr:lsr:lsr:lsr
        ora #16
        sta ($fe),y+
        lda ltime+1
        and #$0f
        ora #16
        sta ($fe),y+
        lda #"."
        sta ($fe),y+
        lda ltime
        lsr:lsr:lsr:lsr
        ora #16
        sta ($fe),y+
        lda ltime
        and #$0f
        ora #16
        sta ($fe),y
    };
end;

procedure BestLap(pnum:byte;time:word);
begin
    g_bestTimes[0] := time;
    PutChar(savmsc + (36 * 40) + 29, char(17 + pnum), %10);
    DisplayBanner(100);
end;

procedure BestTime(p:byte);
begin
    g_bestTimes[g_laps] := p_raceTime[p];
    Write('BEST TIME!'*);
end;

function PlayerCollided(pnum: byte):boolean;
begin
    result := (getPixel(p_X[pnum], p_y[pnum]) = 0);
end;

procedure TurnLeft(pnum: byte);
begin
    if p_angle[pnum] = 0 then p_angle[pnum] := 179
    else dec(p_angle[pnum]);
end;

procedure CrashPlayer(pnum: byte);
begin
    p_state[pnum] := STATE_CRASHED;
    p_tailClear[pnum] := TAIL_LENGTH + 1;
    ShowCrashed(pnum);
end;

procedure HitFinish(pnum:byte);
begin
    p_state[pnum] := STATE_FINISHED;
    p_tailClear[pnum] := TAIL_LENGTH + 1;        
    p_raceTime[pnum] := GetTimeSpan(g_time, p_startFrame[pnum]);
end;

procedure AdvanceLap(pnum: byte);
begin
    Inc(p_lap[pnum]);
    p_lap_delay[pnum] := 5;
    if p_lap[pnum] = 1 then p_startFrame[pnum] := g_time;
    if p_lap[pnum] > 1 then begin
        p_lapTime[pnum] := GetTimeSpan(g_time, p_lapFrame[pnum]);
        if p_lapTime[pnum] < g_bestTimes[0] then BestLap(pnum,p_lapTime[pnum]);
        ShowLapTime(pnum);
    end;
    p_lapFrame[pnum] := g_time;
    
    if p_lap[pnum] > g_laps then HitFinish(pnum)
        else ShowLap(pnum);
end;

procedure SetAudio(pnum, freq, vol: byte);
begin
    aud1[pnum,0] := freq;
    aud1[pnum,1] := vol;
    if isStereo then begin
        aud2[pnum,0] := freq;
        aud2[pnum,1] := vol;
    end;
end;

procedure MovePlayer(pnum: byte);
var vector: shortInt;   
    delta: integer;
    tail,angle: byte;
    moved: boolean;
    freq: byte;
begin
    moved := false;
    
    if p_state[pnum] = STATE_DRIVING then begin
  
        if FirePressed(pnum) then begin                                 // turning
            p_accel[pnum] := ACCEL_DELAY;
            TurnLeft(pnum);    
            if p_turningSpan[pnum] = 0 then begin                      // brake on turn
                if p_speed[pnum] > 0 then dec(p_speed[pnum]);
                p_turningSpan[pnum] := BRAKE_DELAY;
            end else dec(p_turningSpan[pnum]);
        end else begin                          
            p_turningSpan[pnum] := BRAKE_DELAY;
            if p_accel[pnum] = 0 then begin                             // speed up
                if p_speed[pnum] < 3 then inc(p_speed[pnum]);
                p_accel[pnum] := ACCEL_DELAY;                    
            end else dec(p_accel[pnum]);
        end;

        speedDelta := speedTables[p_speed[pnum]];  // select speed table
        angle := p_angle[pnum];
        vector := speedDelta[angle];         // speed on x - axis
        delta := p_speedX[pnum] + vector;
        if vector > 0 then begin           // right
            if delta > GAME_SPEED then begin
                inc(p_X[pnum]);
                moved := true;
                delta := delta - GAME_SPEED;
            end;
            p_speedX[pnum] := delta;
        end;
        if vector < 0 then begin           // left
            if delta < 0 then begin
                dec(p_X[pnum]);
                moved := true;
                delta := delta + GAME_SPEED;
            end;
            p_speedX[pnum] := delta;
        end;
        
        vector := -speedDelta[angle + 45];    // speed on y - axis
        delta := p_speedY[pnum] + vector;
        if vector > 0 then begin           // down
            if delta  > GAME_SPEED then begin
                inc(p_Y[pnum]);
                delta := delta - GAME_SPEED;
                moved := true;
            end;
            p_speedY[pnum] := delta;
        end;
        if vector < 0 then begin           // up
            if delta < 0 then begin
                dec(p_Y[pnum]);
                moved := true;
                delta := delta + GAME_SPEED;
            end;
            p_speedY[pnum] := delta;
        end;
        
        // engine sound
        freq := 200 - (p_speed[pnum] shl 2) - (pnum shl 1);
        aud1[pnum,0]:=freq;
        if isStereo then begin
            aud2[pnum,0]:=freq;
            freq := (p_X[pnum] shr 6) + 2;
            aud2[pnum,1] := $A0 or freq;
            aud1[pnum,1] := $A0 or (6 - freq);
        end else aud1[pnum,1]:= $A4; 
    end;
    
    if moved then begin
        if PlayerCollided(pnum) then CrashPlayer(pnum)
            else DrawPlayer(pnum);
    
        if (p_X[pnum] = 80) and (p_Y[pnum] > 40) and (angle < 90) and (p_lap_delay[pnum] = 0) then
            AdvanceLap(pnum)
    end;
    
    if p_lap_delay[pnum] > 0 then dec(p_lap_delay[pnum]); // tweak to not allow cross finish line twice
    
    // clear tail on crash or finish
    if (p_state[pnum] = STATE_CRASHED) or (p_state[pnum] = STATE_FINISHED) then 
        if (p_tailClear[pnum] > 0) then begin
            ClearTail(pnum);
            Dec(p_tailClear[pnum]);
            tail := p_tailClear[pnum];
            // explode sound
            if p_state[pnum] = STATE_CRASHED then SetAudio(pnum, 60 - tail, $80 or (tail shr 2));
            // win sound
            if p_state[pnum] = STATE_FINISHED then SetAudio(pnum, ((tail and 1) shl 4) + tail, $a0 or (tail shr 1));
        end else p_state[pnum] := STATE_ENDED;
end;


procedure DrawBoard;
var l: byte;
    vsrc,vdest: word;
    s: string[17];
begin
    s := ' '~+'P1'*~+' Lap 0        '~;
    ExpandRLE(RLEDATA, savmsc);
    vsrc := savmsc + 39 * 40;
    vdest := vsrc + 40;
    for l:=40 to 79 do begin
        move(pointer(vsrc),pointer(vdest),40);
        dec(vsrc,40);
        inc(vdest,40);
    end;
    Fillbyte(pointer(txtmsc),160,$5C);
    Fillbyte(pointer(txtmsc),40,0);
    Fillbyte(pointer(txtmsc+82),17,0);
    Fillbyte(pointer(txtmsc+101),17,0);
    for l:=0 to 3 do begin
        move(s[1], pointer(txtmsc - 8 + p_menuOffset[l]), 17);
        inc(s[3]);
    end;
    hscrol:=10;
end;

procedure MovePlayers;
var p:byte;
    endCount:byte;
begin
    endCount := 0;
    for p:=0 to g_players-1 do begin
        MovePlayer(p);
        if p_state[p] = STATE_ENDED then inc(endCount);
    end;
    if endCount = g_players then race_over := true;
end;

procedure InitPlayers;
var p:byte;
begin
    fillByte(@p_tailX, sizeof(p_tailX), NONE);
    for p:=0 to PLAYER_MAX do begin
        p_X[p] := 75;
        p_Y[p] := 59 + (p shl 2);
        p_angle[p] := 45;
        p_tail[p] := 0;
        p_accel[p] := ACCEL_DELAY;
        p_turningSpan[p] := BRAKE_DELAY;
        p_speed[p] := 0;
        p_raceTime[p] := $ffff;
        p_color[p] := 0;
        p_lap[p] := 0;
        p_state[p] := STATE_IDLE;
        if p < g_players then begin
            p_state[p] := STATE_DRIVING;
            DrawPlayer(p);  
        end;
        g_racePositions[p] := p;
    end;
end;

procedure DrawStart();
var pmgmem:word;
    i,b:byte;
begin
    pmgmem:=PMG + $180;
    b := %01;
    for i:=67 to 94 do begin
        poke(pmgmem + i,b);
        b := b xor %11;
    end;
end;

procedure SetDLI();
var dlist,w:word;
    b:byte;
begin
    Pause;
    dlist := sdlstl + 84;   // set DLI
    b := Peek(dlist);
    Poke(dlist, b or 128);
    b := Peek(dlist+1);     // set HSCROLL on timer
    Poke(dlist+1, b or $11);
    w := DPeek(dlist+2);    // adjust memory offset due to hscroll width
    DPoke(dlist+2, w-8);
    SetIntVec(iDLI, @dli);
    nmien := $c0; 
end;

procedure InitPMG;
begin
    PMG_Init(Hi(PMG));
    FillByte(pointer(PMG + (3 * $80)),5 * $80,0);
    PMG_hposm0 := 127;
    PMG_sizem := PMG_SIZE_NORMAL;
    PMG_sizep1 := PMG_SIZE_x4;
    PMG_sizep2 := PMG_SIZE_x4;
    PMG_sizep3 := PMG_SIZE_x4;
    FillByte(pointer(PMG + 640 + 52),8,$ff);
    FillByte(pointer(PMG + 768 + 52),8,$ff);
    FillByte(pointer(PMG + 896 + 52),8,$ff);
    PMG_gprior_S := 1;
end;

procedure ShowGameScreen;
begin
    fadeTo(BLACK);
    Pause;
    InitGraph(7);
    color0 := 0;
    color1 := 0;
    color2 := 0;
    color3 := 0;
    color4 := 0;
    chbas := Hi(TITLEFONT); // set custom charset;
    DrawBoard;
    InitPMG;
    InitPlayers;
    DrawStart;
    SetDLI();
    fadeTo(GAME);
end;

procedure HidePMG;
begin
    PMG_Disable;
end;

function AnyFirePressed:boolean;
begin
    result := FirePressed(0) or FirePressed(1) or FirePressed(2) or FirePressed(3) or (consol and 1 = 0);
end;

procedure WaitForFireOrStart;
var c:char;
begin
    repeat 
        Pause;
        if Keypressed then begin
            c := ReadKey;
            if byte(c) = 27 then begin
                escPressed := true;
            end;
        end;
    until AnyFirePressed or escPressed;
    repeat Pause until not AnyFirePressed or escPressed;
end;

procedure InvertChars(mem:word;count:byte);
begin
    while count > 0 do begin
        poke(mem,peek(mem) xor 128);
        inc(mem);
        dec(count);
    end;
end;


procedure ShowBestLap(row:byte);
var mram:word;
begin
    if g_bestTimes[0] = $ffff then exit;
    mram := savmsc + 40 * (row-1);
    Fillbyte(pointer(mram+9),22,$5c);
    Gotoxy(11,row); Write('  Best Lap : ',GetTimeStr(g_bestTimes[0]),'  ');
    InvertChars(mram+10,20);
end;


procedure UpdateMenu;
var len:byte;
    s:TString;
begin
    len:= 4 + ((g_players - 1) * 5);
    FillByte(pointer(MENURAM + 179),17,0);
    Move(playersMenu[1],pointer(MENURAM + 179),len);

    FillByte(pointer(MENURAM + 259),17,0);
    s := controllers[g_controller];
    if (g_controller = CONTROL_MULTIJOY) then len:=14;
    Move(s[1],pointer(MENURAM + 259),len);

    Gotoxy(20,9);
    write(g_laps,' ');

    Gotoxy(25,9);
    if g_bestTimes[g_laps]<>$ffff then begin
        s:=GetTimeStr(g_bestTimes[g_laps]);
             Write('Record: ',s);
    end else Write('             ');

    Gotoxy(20,11);
    write(gameModes[g_mode]);
    
    if g_mode = MODE_MATCH then begin
        Gotoxy(27,11);
        Write(g_matchRaces2Win);
    end;
    if g_mode = MODE_RACE4POINTS then begin
        Gotoxy(27,11);
        Write(g_matchPoints2Win);
    end;
    
    
end;

procedure ShowTitle;
var mram:word;
    b:byte;
    c:char;
begin
    FadeTo(WHITE);
    Pause;
    cursoroff;
    chbas := Hi(TITLEFONT); // set custom charset;
    SDLSTL := TITLEDLIST;
    nmien := $40;
    savmsc := MENURAM;
    mram := MENURAM + 78;
    FillByte(pointer(MENURAM), 18*40 ,0);
    
    for b:=0 to 13 do begin                 // draw flag border
        FillByte(pointer(mram), 4, $5b);
        Inc(mram,40);
    end;
    FillByte(pointer(MENURAM + 40), 2*40, $5b);
    FillByte(pointer(MENURAM + 40 * 15), 2*40, $5b);

    Gotoxy(4,5);  Write('No.of '+'P'*+'layers :');
    Gotoxy(4,7);  Write('C'*'ontrollers   :');
    Gotoxy(4,9);  Write('No.of '+'L'*+'aps    :');
    Gotoxy(4,11); Write('G'*'ame Mode     :');
    Gotoxy(7,14); Write('Press '+'FIRE'*+' or '+'START'*' to race!');
    UpdateMenu;
    ShowBestLap(17);
    pause;
    msx.Init(0);
    musicOn:=1;
    scrollx:=0;
    scrollh:=8;
    SetScrollX(scrollx);
    scrollOn:=1;
    
    FadeTo(TITLE);
    escPressed := false;
        
    repeat                              // MENU LOOP
        Pause;
        if keypressed then begin 
            c := ReadKey;
            case c of
                'P','p': begin 
                    inc(g_players);
                    if (g_controller = CONTROL_NORMAL) and (g_players = 4) then g_players := 1;
                    if (g_controller = CONTROL_MULTIJOY) and (g_players = 5) then g_players := 1;
                end;
                'C','c': begin 
                    inc(g_controller);
                    if g_controller = 2 then g_controller := 0;
                    if (g_controller = CONTROL_NORMAL) and (g_players > 3) then g_players := 3;
                end;
                'L','l': begin 
                    inc(g_laps);
                    if g_laps = 10 then g_laps := 1;
                end;
                'G','g': begin 
                    inc(g_mode);
                    if g_mode = 3 then g_mode := 0;
                end;
                'M','m': begin 
                    if g_mode = MODE_MATCH then begin
                        inc(g_matchRaces2Win);
                        if g_matchRaces2Win = 26 then g_matchRaces2Win := 3;
                    end;
                    if g_mode = MODE_RACE4POINTS then begin
                        inc(g_matchPoints2Win);
                        if g_matchPoints2Win = 101 then g_matchPoints2Win := 3;
                    end;
                end;
                #27: g_mode := MODE_QUIT;
            end;
            UpdateMenu;
        end;
    until AnyFirePressed or (g_mode = MODE_QUIT);
    scrollOn:=0;
end;

procedure TimeStep();
begin
    asm {
        sed
        lda g_time 
        clc
        adc #1
        sta g_time
        cmp #$50
        bne @+
        lda #0
        sta g_time
        lda g_time+1
        clc
        adc #1
        sta g_time+1
@    
        cld
    };
end;

procedure AddScore(p, place:byte);
begin
    case place of
        0: Inc(g_matchPoints[p],3); // 1st
        1: Inc(g_matchPoints[p],2); // 2nd
        2: Inc(g_matchPoints[p],1); // 3rd
    end;
end;

procedure SwapMatchPositions(p: byte);
var b: byte;
begin
    b := g_matchPositions[p];
    g_matchPositions[p] := g_matchPositions[p+1];
    g_matchPositions[p+1] := b;
end;

procedure SwapRacePositions(p: byte);
var b: byte;
begin
    b := g_racePositions[p];
    g_racePositions[p] := g_racePositions[p+1];
    g_racePositions[p+1] := b;
end;

procedure SortMatchPositions;
var c,p,n,count:byte;
begin
    if g_players > 1 then 
    for count := 0 to g_players-2 do
        for p := 0 to g_players-2 do begin
            c := g_matchPositions[p];
            n := g_matchPositions[p + 1];
            if (g_matchPoints[c] < g_matchPoints[n]) then SwapMatchPositions(p);
        end;
end;

procedure SortRacePositions;
var c,p,n,count:byte;
begin
    if g_players > 1 then 
    for count := 0 to g_players-2 do
        for p := 0 to g_players-2 do begin
            c := g_racePositions[p];
            n := g_racePositions[p + 1];        
            if p_raceTime[c] > p_raceTime[n] then SwapRacePositions(p);
        end;
end;

procedure ShowResults;
var mram:word;
    row:byte;
    p:byte;
    place:byte;
    points:byte;
    prev_time:word;
    prev_points:byte;
    score_reached:boolean;
begin
    pause;
    msx.Init(2);
    musicOn := 1;
    HidePMG;
    FadeTo(WHITE);
    Pause;
    InitGraph(0);
    color2 := $0f;
    color1 := $0f;
    color4 := $0f;
    chbas := Hi(TITLEFONT); // set custom charset;
    nmien := $40;
    score_reached := false;
    CursorOff;
    mram := savmsc + 318;
    for row:=0 to 14 do begin                 // draw flag border
        FillByte(pointer(mram), 4, $5b);
        Inc(mram,40);
    end;
    mram := savmsc;
    FillByte(pointer(mram + 40 * 6), 2*40, $5b);
    FillByte(pointer(mram + 40 * 22), 2*40, $5b);
    
    place := 0;
    prev_time := 0;
    SortRacePositions;
    
    for row:=0 to g_players-1 do begin
        p:=g_racePositions[row];
        Gotoxy(5,11 + row * 2);
        if p_raceTime[p] = $ffff then begin
            Write(places[4]);
            Write('P'*,char(128+p+49),' - ');
            Write(places[5]);
        end else begin
            if prev_time <> p_raceTime[p] then place := row;
            Write(places[place]);
            Write('P'*,char(128+p+49),' - ');
            Write(GetTimeStr(p_raceTime[p]),' ');
            if p_raceTime[p] < g_bestTimes[g_laps] then BestTime(p);
            prev_time := p_raceTime[p];
            AddScore(p, place);
        end;
        if g_matchPoints[p] >= g_matchPoints2Win then score_reached := true;
    end;

    Gotoxy(5,21); Write('Press '+'FIRE'*+' or '+'START'*' to continue!');
    Move(raceResultsLogo, pointer(savmsc + 2), sizeof(raceResultsLogo));
    ShowBestLap(24);

    FadeTo(TITLE);
    
    if g_mode <> MODE_QUICKRACE then begin   // ************* MATCH RESULTS
        
        WaitForFireOrStart;
        FadeTo(White);
        mram := savmsc + 40 * 9 + 3;
        for row := 0 to 9 do begin
             FillByte(pointer(mram), 34, 0);
             inc(mram,40);
        end;
        mram := savmsc + 40 * 6;
        FillByte(pointer(mram+2), 19, $5c);
        FillByte(pointer(mram+42), 19, $5c);
        FillByte(pointer(mram+3), 17, 0);
        FillByte(pointer(mram+43), 17, 0);

        
        Gotoxy(5,7); Write('Race no. ',g_matchCount);

        Gotoxy(5,8);
        if g_mode = MODE_MATCH then begin 
            if g_matchCount = g_matchRaces2Win then begin
                match_over := true;
                Writeln('FINAL RESULTS:');
            end else begin
                Writeln(g_matchRaces2Win - g_matchCount,' to go');
            end;
        end;
        if g_mode = MODE_RACE4POINTS then begin 
            if score_reached then match_over := true;
            Writeln(g_matchPoints2Win,' points to win');
        end;
        InvertChars(mram+3,17);
        InvertChars(mram+43,17);

        Move(matchResultsLogo, pointer(savmsc), sizeof(matchResultsLogo));
        
        place := 0;
        prev_points := 0;
        SortMatchPositions;
                
        for row := 0 to g_players - 1 do begin
            p := g_matchPositions[row];
            points := g_matchPoints[p];
            Gotoxy(5, 11 + row * 2);
            if prev_points <> points then place := row;
            Write(places[place]);
            Write('P'*,char(128+p+49),' - ');
            Write(points);
            Write(' point');
            if points <> 1 then write('s');
            prev_points := points;
        end;    
    
        FadeTo(TITLE);
        escPressed := false;
    
    end else match_over := true;
end;

procedure InitGame();
var l:byte;
begin
    PreCalc;
    for l:=0 to 9 do g_bestTimes[l] := $ffff;
    TextMode(0);
    isStereo := DetectStereo;
    Pause;
    GetIntVec(iVBL, oldvbl);
    SetIntVec(iVBL, @vbl);
    msx.player := pointer(RMT_PLAYER_ADDRESS);
    msx.modul := pointer(RMT_MODULE_ADDRESS);    
end;

procedure InitMatch;
var p:byte;
begin
    InitControllers(g_controller);
    for p:=0 to PLAYER_MAX do begin
        g_matchPoints[p] := 0;
        g_matchPositions[p] := p;
    end;
    g_matchCount := 0;
    match_over := false;
    escPressed := false;
end;

procedure InitRace;
begin
    pause;
    msx.Init(9);
    musicOn:=0;
    ShowGameScreen;
    race_over := false;
    g_time := 0;
    escPressed := false;
    Inc(g_matchCount);
end;

procedure ShowRaceInfo;
var slideCounter:byte;
    slide,c,d:byte;
    s1,s2:string[11];
begin
    c := g_matchCount;
    if g_mode = MODE_QUICKRACE then s1 := 'Quick  Race'~
    else begin
        s1 := 'Match No.  '~;
        d := 0;
        while c>9 do begin
            Dec(c,10);
            Inc(d);
        end;
        if d <> 0 then s1[10] := char(d + 16);
        s1[11] := char(c + 16);
    end;
    
    s2:='  0  Laps  '~;
    if g_laps = 1 then s2[9] := ' '~;
    s2[3] := char(g_laps + 16);
    slide := 0;

    repeat
        Pause;
        if slideCounter>0 then dec(slideCounter);
        if slideCounter = 0 then begin
            HideBanner;
            if slide and 1 = 0 then Banner(s1)
                else Banner(s2);
            inc(slide);
            slideCounter := 100;
            Pause;
            ShowBanner;
        end;
    until AnyFirePressed;
    Pause;
    HideBanner;
    Banner('GET READY !'~);
    Pause;
    ShowBanner;
    repeat until not AnyFirePressed;
    HideBanner;
    Banner('BEST LAP P'~);
end;

procedure StartEngineSounds;
begin
    audctl := 1;
    skstat := 3;
    if isStereo then begin
        audctl2 := 1;
        skstat2 := 3;
    end;
    SetAudio(0,160,$C4);
end;

begin
    InitGame;

    repeat  
        ShowTitle;

        if g_mode <> MODE_QUIT then begin  // start match
            InitMatch;
            repeat                   
                InitRace;
                StartEngineSounds;
                ShowRaceInfo;
                repeat // main race loop
                    Pause;
                    MovePlayers;
                    TimeStep;
                    CheckBanner;
                    ShowGameTime;
                until race_over;
                ShowResults;
                WaitForFireOrStart;
            until match_over or escPressed;
        end;

    until g_mode = MODE_QUIT;

    Pause;
    SetIntVec(iVBL, oldvbl);
    nmien:=40;
    TextMode(0);
    Writeln('Thanks for playing.');
    
    if false then begin msx.Play; MoveScroll; end; // dirty hack to compile those procs (called from asm)
end.
